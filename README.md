# ansible-ci-demo

Ansible CI実践デモ用プロジェクト

## Hands-on 手順

### 1. プロジェクト準備

#### 1.1. GitLab.com にログイン
予め作成しておいたアカウントを使用して、 https://gitlab.com/users/sign_in からGitLab.comにログインしましょう。

ユーザーアカウントが未作成の場合は、 https://gitlab.com/users/sign_up からユーザー作成を行ってください。なお、アカウント作成の際にはメールアドレスの存在確認が必要となります。

#### 1.2. 元プロジェクトをフォーク
GitLab上でCIパイプラインの実行やコードの編集をできるように、以下の手順で当プロジェクトのフォーク（派生プロジェクト）を自分のアカウント配下に作成しましょう。

1. ブラウザから https://gitlab.com/hh-sandbox/ansible-ci-demo にアクセスして、*画面右上にある **Fork** ボタンをクリック
2. 遷移した **Fork project** ページで自分のアカウントに表示されている **Select ボタン**を選択
3. フォークしたプロジェクト https://gitlab.com/{あなたのユーザーID}/ansible-ci-demo に移動していることを確認

### 2. GitLab CI Pipeline 実行
次に、フォークしたプロジェクトの中で実際にCI Pipelineを実行して結果を確認してみましょう。

#### 2.1. Pipeline実行
Fork直後はPipelineは自動実行されないため、以下の手順でPipelineをマニュアル実行しましょう。

1. プロジェクト左メニューから **CI/CD** > **Pipelines** を選択
    ![cicd_pipelines](images/cicd_pipelines.png)
2. 右上青色の**Run pipeline** をクリックし、遷移した画面で再び **Run pipeline** ボタンをクリック（実行ブランチと変数はデフォルトのままでOK）
3. 以下のようにPipeline実行が開始されることを確認
    ![runnning_pipeline](images/running_pipeline.png)

実行されたPipelineの履歴は **CI/CD** > **Pipelines** からいつでも確認することが可能です。

#### 2.2. Pipeline中ジョブ実行ログ確認
上記の実行中Pipeline画面で、*ansible29-lint* をクリックすると、以下のようなジョブ実行ログが表示されます。

![job_log](images/job_log.png)

ジョブが実行完了するまで、ログ出力やCI定義を見ながら、しばし待ちましょう。


### 3. CI定義確認

#### 3.1. gitlab-ci.yml
GitLabでは、CI定義は常に [.gitlab-ci.yml](./.gitlab-ci.yml) に定義されています。まずは、このファイルを見て、どのようなCI Pipelineが定義されているか確認してみましょう（.gitlab.ci.yml のリファレンスは [こちら](https://docs.gitlab.com/ee/ci/yaml/README.html)）。

このファイル中では以下のように、lint, testという2つのステージに分かれて合計3つのCI Jobが定義されています。

* *lint stage*:
  * ansible29-lint
    * Ansible 2.9を使ってmolecule経由でansible lintを実行してapache roleの静的解析を行う
* *test stage*:
  * *py36-ansible29-test*
    * Python 3.6 + Ansible 2.9でmoleculeからapache roleの動作テストを実施
  * *py38-ansible29-test*
    * 上記の Python 3.8版

(*.ansible29-test* のように `.` 始まりの定義は非実行対象となるため、共通テンプレートとして活用することができます)

#### 3.2. Molecule定義確認
Molecule実行定義については、[roles/apache/molecule/default/molecule.yml](roles/apache/molecule/default/molecule.yml) に定義されています。今回のHands-onでは、Moleculeの細かい設定までの深掘りは行いませんが、以下のポイントを押さえておいてください。

* テスト環境はdocker driverを使用して、UBI8イメージを使用したコンテナとしてデプロイされる
* Moleculeでは任意のコマンドをLint工程で実行することが可能
  * 今回は ansible-lint コマンドを実行しているのみ
* テスト環境のデプロイ定義Playbookは [converge.yml](roles/apache/molecule/default/converge.yml)
* テスト(=動作確認)Playbookは [verify.yml](roles/apache/molecule/default/verify.yml)


### 4. 修正Merge Request作成

#### 4.1. Lint実行エラー確認
しばらく待つと、2. で実行したジョブの実行が完了しますが、残念ながら実行失敗(failed)ステータスになってしまっているはずです。ジョブの実行ログをみて、どこに問題があったのか確認してみましょう。

![lint_error](images/lint_error.png)

ログ下部を見てみると、以下の ansible-lint 規約違反がエラーの原因であることが分かります。

```
WARNING  Listing 1 violation(s) that are fatal
package-latest: Package installs should not use latest
tasks/main.yml:2 Task/Handler: Ensure apache is installed
```

メッセージに書かれている通りですが、[yum module](https://docs.ansible.com/ansible/2.9/modules/yum_module.html) を始めとするパッケージ管理モジュールでの `latest` を指定した最新版インストールを禁止するルールがデフォルトでansible-lintに含まれており(「最新版」という指定が曖昧かつ、想定外タイミングでアップデートが実施されてしまう恐れが伴うため)、この `latest` が [tasks/main.yml 2行目の Ensure apache is installed タスク](roles/apache/tasks/main.yml#L2) で使用されていることが規約違反となっています。

それでは、上記で確認したエラーを実際に修正して、Merge Requestを作成してみましょう。

#### 4.2. ブラウザ上でコードを修正
通常の開発作業であれば、コードの修正はローカル環境のエディタで実施した上で、各種Gitクライアント経由でGitLabに反映することになりますが、以下のようにブラウザから直接GitLabで管理されているファイルを編集することも可能です。

1. [roles/apache/tasks/main.yml](roles/apache/tasks/main.yml) をブラウザ上で開く
2. **Edit** ボタンをクリックして編集モードに入る
    ![edit_file](images/edit_file.png)
3. ファイル 5 行目を以下のように修正 :warning: インデントがずれないように注意!!
    ```yaml
        state: present
    ```
    （`present` はインストール状態であることのみを指定するパラメータ）

#### 4.3. *fix-lint* ブランチに変更をコミット
エディタ下部にスクロールして、以下のようにコミット情報を入力してください。

* **Commit message**: `fix package-latest lint warning`
* **Target Branch**: `fix-lint`

GitLab Flowに基づいて、`main` ブランチはMerge Request経由でレビュー済みの変更のみを取り込む方針とするため、必ず Target Branchを `main` から変更するようにしてください（実運用時は [Protected branch](https://docs.gitlab.com/ee/user/project/protected_branches.html) を設定して特定ブランチへの直接のコミット・プッシュを禁止する設定を行うことになります）。

![commit](images/commit.png)

上記の通り入力したら **Commit changes** ボタンをクリックして変更を確定しましょう。

#### 4.4. Merge Request作成
新しいブランチに対してコミットを実施すると、自動的にMerge Request作成ページに移動します。様々な編集可能項目がありますが、ここではそのまま画面下部までスクロールして **Create merge request** ボタンをクリックしてください。

これで *main* ブランチへのMerge Requestが作成されました。

### 5. Pipeline成功確認 & Merge

#### 5.1. Pipeline成功確認
Merge Requestが作成されると、自動でPipelineの実行も開始され、Merge Requestの詳細画面から状態を確認する事ができます。

今回のサンプルRoleはMoleculeテストは最初から正常に動く状態になっていますので、先程のLintエラーの修正さえ正しくできていれば、最終的には以下のようにPipeline中の全ジョブが正常完了して成功ステータスになるはずです。

![mr_pipeline](images/mr_pipeline.png)

#### 5.2. 変更内容レビュー
Pipelineが実行完了するのを待つ間に、変更内容のレビューを行っておきましょう。Merge Requestページの **Changes** タブから、具体的な今回の変更内容を確認する事が可能です。

![changes](images/changes.png)]

#### 5.3. mainブランチへのMerge
Pipelineの実行が成功し、変更箇所が想定通りであることも確認できたら、Mergeボタンをクリックして今回加えた修正を *main* ブランチに取り込んであげましょう。

![merge](images/merge.png)]

今回は1名実施のハンズオンのため、実装者=レビュアーとなっていますが、実際にはプロジェクト内のアクセス権限管理を行い、開発者とレビュアーをしっかり分離することが重要です。GitLabの場合、Merge時のコードレビューを必須としたり、レビュー承認者を細かく指定するなどの設定も可能となっています（[Merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/), 一部機能は有償版が必要）。

ここまでで、簡単な内容ではありましたが、

* GitLabを使用したAnsible Playbook/RoleのCI
* Merge Requestを使用したGitを中心とした開発フロー

の概要をつかんでいただけたかと思います。

当Hands-onは以上で完了となります。お疲れさまでした！！

## 参考資料
* GitLab CI/CD 公式ドキュメント(英語): https://docs.gitlab.com/ee/ci/
* 上記の日本語訳(クリエーションライン株式会社作成): https://gitlab-docs.creationline.com/ee/ci/README.html
  * GitLab公式ではなく情報も最新とは限らない点に注意
* Ansible Lint Documentation: https://ansible-lint.readthedocs.io/en/latest/
* Ansible Molecule: https://molecule.readthedocs.io/en/latest/
